from math import pi


while True:
    height = input("Введите высоту 'H': ").strip().replace(",", ".")
    radius = input("Введите радиус 'R': ").strip().replace(",", ".")

    if not (
        (radius.replace(".", "", 1).isdigit() and height.replace(".", "", 1).isdigit())
        and (
            (0 < (radius := float(radius)) < 264)
            and (0 < (height := float(height)) < 264)
        )
    ):
        print("Введено неверное значение H или R")
        continue

    break

result = round((pi * (radius**2) * height) / 1000, 2)
print(f"Объем стакана 'V'={result} л.")
